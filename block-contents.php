<?php
?>

<?php
$the_resource_url = get_field('resource_url', get_the_ID());

?>

<h2><a href="<?php echo $the_resource_url; ?>"><?php the_title(); ?></a></h2>

